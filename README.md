# TAGs #

Segunda yh6yhyh6yh6yh6hy66hy6

### Lista de versiones ###

* https://bitbucket.org/upc-unl/testeando-bb/downloads/?tab=tags

### Versiones ###

* https://bitbucket.org/upc-unl/testeando-bb/get/0.0.1.tar.gz
* https://bitbucket.org/upc-unl/testeando-bb/get/0.0.2.tar.gz


### howto ###

Dos formas de hacer un tag en bitbucket

* Despuès de un commit
* Buscando el último commit

En cualquiera de las opciones te aparece un link **+tag** que permite crearlo. Se ingresa el nombre, generalmente x.y.z, donde habrá una copia completa de todo el repositorio al momento del tagging. 